import React, {createContext, useState, FC, ReactNode} from 'react';
import {AuthContextType, User} from '../@types/auth'; 
import {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {login} from '../store/actions';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {SUCCESS} from '../store/actionsType';
import {loginReset} from '../store/auth/actions';
export const AuthContext = createContext<AuthContextType | null>(null);

type AuthProviderType = {
  children: ReactNode;
};
const AuthProvider: FC<AuthProviderType> = ({children}) => {
  const dispatch = useDispatch();
  const {isLoadingActionLogin, statusLogin, userData} = useSelector(
    state => state.auth,
  );
  const [user, setUser] = useState<User>(null);

  useEffect(() => {
    getAuthState();
  }, []);

  useEffect(() => {
    if (statusLogin === SUCCESS) {
      setAuth(userData);
      dispatch(loginReset({}));
    }
  }, [statusLogin]);

  const getAuthState = async () => {
    try { 
      const userString = await AsyncStorage.getItem('user');
      const userData = JSON.parse(userString || null);
      console.log(userData);
      setUser(userData);
    } catch (err) {
      setUser(null);
    }
  };

  const auth = (payload: any) => {
    dispatch(login(payload));
  };
  const setAuth = async (user: User) => {
    try {
      await AsyncStorage.setItem('user', JSON.stringify(user));
      setUser(user);
    } catch (error) {
      Promise.reject(error);
    }
  };

  const logout = async () => {
    try {
      const userString = await AsyncStorage.removeItem('user');
      setUser(null);
    } catch (err) {
      setUser(null);
    }
  };

  return (
    <AuthContext.Provider value={{user, auth, setAuth, logout}}>
      {children}
    </AuthContext.Provider>
  );
};

export default AuthProvider;
