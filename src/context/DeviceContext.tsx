import React, {createContext, useState, FC, ReactNode} from 'react';
import {DeviceContextType, Device} from '../@types/device';
import {Platform} from 'react-native';
import DeviceInfo from 'react-native-device-info';
import {useEffect} from 'react';
import {PERMISSIONS, request, RESULTS} from 'react-native-permissions';
import IMEI from './importIMEI';


export const DeviceContext = createContext<DeviceContextType | null>(null);

type DeviceProviderType = {
  children: ReactNode;
};
const DeviceProvider: FC<DeviceProviderType> = ({children}) => {
  const [deviceInfo, setDeviceInfo] = useState<Device>({
    deviceName: '',
    imei: '',
    ip: '',
    os: '',
    mac: '',
  });

  useEffect(() => {
    getDeviceInfo();
  }, []);
  const getDeviceInfo = async () => {
    setDeviceInfo(prevState => ({
      ...prevState,
      os: Platform.OS,
    }));

    DeviceInfo.getDeviceName().then(deviceName => {
      setDeviceInfo(prevState => ({
        ...prevState,
        deviceName,
      }));
    });
    DeviceInfo.getMacAddress().then(mac => {
      setDeviceInfo(prevState => ({
        ...prevState,
        mac,
      }));
    });
    DeviceInfo.getIpAddress().then(ip => {
      setDeviceInfo(prevState => ({
        ...prevState,
        ip,
      }));
    });
    if (Platform.OS === 'android') {

       
      // await request(PERMISSIONS.ANDROID.READ_PHONE_STATE)
      //   .then(result => {
      //     switch (result) {
      //       case RESULTS.UNAVAILABLE:
      //         console.log(
      //           'This feature is not available (on this device / in this context)',
      //         );
      //         break;
      //       case RESULTS.DENIED:
      //         console.log(
      //           'The permission has not been requested / is denied but requestable',
      //         );
      //         break;
      //       case RESULTS.LIMITED:
      //         console.log(
      //           'The permission is limited: some actions are possible',
      //         );
      //         break;
      //       case RESULTS.GRANTED:
      //         console.log('The permission is granted');

      //         break;
      //       case RESULTS.BLOCKED:
      //         console.log(
      //           'The permission is denied and not requestable anymore',
      //         );
      //         break;
      //     }
      //   })
      //   .catch(error => {
      //     // …
      //   });
      // try {
      //   console.log(IMEI);
      //   IMEI.getImei().then(result=>{

      //     if (result && result.length >0) {
      //         var deviceIMEI = result;
      //     }
      // });
      // } catch (error) { 
      //   console.log(error);
      // }
    }
  };
  return (
    <DeviceContext.Provider value={{deviceInfo}}>
      {children}
    </DeviceContext.Provider>
  );
};

export default DeviceProvider;
