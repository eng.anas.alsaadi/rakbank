import React, {createContext, useState, ReactNode, FC} from 'react';
import {LocationContextType, Location} from '../@types/location';
import {Platform, PermissionsAndroid, Alert} from 'react-native';
import {PERMISSIONS, request, RESULTS} from 'react-native-permissions';
import Geolocation from 'react-native-geolocation-service';
import {useEffect} from 'react';

export const LocationContext = createContext<LocationContextType | null>(null);

type LocationProviderType = {
  children: ReactNode;
};

const LocationProvider: FC<LocationProviderType> = ({children}) => {
  const [currentLocation, setCurrentLocation] = useState<Location>(null);

  useEffect(() => {
    getCurrentLocation();
  }, []);

  const requestLocationPermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        );
      } catch (err) {}
    } else {
      request(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE).then(result => {
        switch (result) {
          case RESULTS.UNAVAILABLE:
            console.log(
              'This feature is not available (on this device / in this context)',
            );

            break;
          case RESULTS.DENIED:
            Alert.alert(
              'Please go to setting and enable get current location update',
            );
            break;
          case RESULTS.LIMITED:
            console.log('The permission is limited: some actions are possible');
            break;
          case RESULTS.GRANTED:
            break;
          case RESULTS.BLOCKED:
            Alert.alert(
              'Please go to setting and enable get current location update',
            );
            break;
        }
      });
    }
  };

  const getCurrentLocation = async () => {
    await requestLocationPermission();
    Geolocation.getCurrentPosition(
      position => {
        const region = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: 0.003,
          longitudeDelta: 0.003,
        };

        setCurrentLocation(region);
      },
      error => {},
      {enableHighAccuracy: false, timeout: 200000, maximumAge: 5000},
    );
  };

  return (
    <LocationContext.Provider value={{currentLocation}}>
      {children}
    </LocationContext.Provider>
  );
};

export default LocationProvider;
