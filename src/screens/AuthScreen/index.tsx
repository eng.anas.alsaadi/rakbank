import React, {useContext, useEffect, useState} from 'react';
import {View, StyleSheet, BackHandler, Keyboard} from 'react-native';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import Animated, {
  useSharedValue,
  useAnimatedStyle,
  withSpring,
  interpolate,
  Extrapolation,
  interpolateColor,
  useAnimatedKeyboard,
} from 'react-native-reanimated';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import MText from '../../components/global/elements/Text';
import colors from '../../constants/colors';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MButton from '../../components/global/elements/Button';
import Layout from '../../components/template/layout';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {useDispatch, useSelector} from 'react-redux';
import MTextInput from '../../components/global/elements/TextInput';
import {LocationContext} from '../../context/LocationContext';
import {DeviceContext} from '../../context/DeviceContext';
import {AuthContext} from '../../context/AuthContext';
import {useTranslation} from 'react-i18next';

const AuthScreen = ({}) => {
  const config = {
    duration: 2000,
  };
  const {currentLocation} = useContext(LocationContext);
  const {deviceInfo} = useContext(DeviceContext);
  const {auth} = useContext(AuthContext);
  const {t} = useTranslation();

  const dispatch = useDispatch();
  const {isLoadingActionLogin} = useSelector(state => state.auth);

  const [userId, setUserId] = useState('');
  const [password, setPassword] = useState('');
  const inputRef = React.createRef();
  const formVisible = useSharedValue(0);
  const keyboardAnimation = useAnimatedKeyboard();

  const handleBackButtonClick=()=> {
    
    return true;
  }
  
  useEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener("hardwareBackPress", handleBackButtonClick);
    };
  }, []);
  const styleForTransform = useAnimatedStyle(() => {
    if (keyboardAnimation && keyboardAnimation.state.value == 3) {
      formVisible.value = withSpring(0);
    }
    return {
      transform: [{translateY: -keyboardAnimation.height.value / 1.1}],
    };
  });

  const styleForTransformLayout = useAnimatedStyle(() => {
    return {
      transform: [{translateY: -keyboardAnimation.height.value / 1.4}],
    };
  });

  const animatedStylesOpacity = useAnimatedStyle(() => {
    const opacity = interpolate(formVisible.value, [0, 1], [1, 0], {
      extrapolateRight: Extrapolation.CLAMP,
    });
    return {
      opacity: opacity,
    };
  });

  const animatedStylesOpacityReverse = useAnimatedStyle(() => {
    const opacity = interpolate(formVisible.value, [0, 1], [0, 1], {
      extrapolateRight: Extrapolation.CLAMP,
    });
    return {
      opacity: opacity,
    };
  });

  const animatedStylesBackground = useAnimatedStyle(() => {
    const backgroundC = interpolateColor(
      formVisible.value,
      [0, 1],
      ['rgb(58,56,60)', 'rgb(144,140,144)'],
    );
    return {
      backgroundColor: backgroundC,
    };
  });

  const onPressLoginButton = () => {
    if (formVisible.value === 1) {
      if (userId.length === 0 || password.length === 0) {
        console.log(currentLocation);
        console.log(deviceInfo);
      } else {
        auth({
          user_id: userId,
          password: password,
          ...deviceInfo,
          ...currentLocation,
        });
      }
    } else {
      formVisible.value = withSpring(1);
      inputRef.current.focus();
    }
  };

  return (
    <Layout styleY={styleForTransformLayout}>
      <View style={{flex: 1}}>
        <View style={styles.HeaderContainer}>
          <Animated.View style={[animatedStylesOpacityReverse]}>
            <TouchableWithoutFeedback
              onPress={() => {
                formVisible.value = withSpring(0);
                Keyboard.dismiss();
              }}>
              <View>
                <MaterialIcons
                  name={'arrow-back-ios'}
                  size={wp(5)}
                  color={colors.white}
                />
              </View>
            </TouchableWithoutFeedback>
          </Animated.View>

          <MButton
            style={{width: wp(25), borderRadius: wp(4)}}
            fontType={'R_4'}
            onPress={() => {}}
            text={'Register'}
            type={'outline'}
          />
        </View>
        <Animated.View
          style={[
            styles.TextContainer,
            animatedStylesOpacity,
            styleForTransform,
          ]}>
          <MText
            type={'B_9'}
            text={t('rakbank')}
            left
            moreStyle={{
              marginBottom: wp(3),
            }}
          />
          <MText
            type={'R'}
            text={t('subtitle')}
            left
            moreStyle={{
              width: wp(70),
              lineHeight: wp(7),
            }}
          />
        </Animated.View>
      </View>

      <View style={{}}>
        <Animated.View
          style={[
            {width: wp(88), opacity: 0},
            animatedStylesOpacityReverse,
            styleForTransform,
          ]}>
          <MTextInput
            inputRef={inputRef}
            title={t('userId')}
            onChangeText={value => setUserId(value)}
            value={userId}
          />

          <MTextInput
            title={t('password')}
            onChangeText={value => setPassword(value)}
            value={password}
            secureTextEntry
          />
        </Animated.View>
        <Animated.View
          style={[
            {
              borderRadius: wp(6),
              marginTop: wp(5),
              backgroundColor: colors.black,
            },
            animatedStylesBackground,
            styleForTransform,
          ]}>
          <MButton
            style={{
              width: wp(88),
              paddingVertical: wp(6),
              backgroundColor: 'transparent',
            }}
            loading={isLoadingActionLogin}
            onPress={onPressLoginButton}>
            <Animated.Text
              style={[
                {
                  position: 'absolute',
                  color: colors.white,
                  fontFamily: 'Montserrat-Regular',
                  fontSize: wp(4),
                },

                animatedStylesOpacity,
              ]}>
              {t('login')}
            </Animated.Text>
            <Animated.Text
              style={[
                {
                  position: 'absolute',
                  color: colors.white,
                  fontFamily: 'Montserrat-Regular',
                  fontSize: wp(4),
                },
                animatedStylesOpacityReverse,
              ]}>
              {t('submit')}
            </Animated.Text>
          </MButton>
        </Animated.View>

        <View style={{position: 'relative', marginBottom: wp(10)}}>
          <Animated.View
            style={[
              styles.TextFotterContainer,

              {flexDirection: 'column'},
              styleForTransform,
              animatedStylesOpacityReverse,
            ]}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginBottom: wp(1),
              }}>
              <MText
                type={'R_3'}
                text={t('forgotUserId')}
                color={colors.black}
              />
              <View
                style={{
                  marginHorizontal: wp(2),
                  width: wp(0.2),
                  height: wp(4),
                  backgroundColor: colors.black,
                }}
              />
              <MText
                type={'R_3'}
                text={t('forgotPassword')}
                color={colors.black}
              />
            </View>

            <MText type={'R_3'} text={t('enableUserId')} color={colors.black} />
          </Animated.View>

          <Animated.View
            style={[
              styles.TextFotterContainer,
              animatedStylesOpacity,
              styleForTransform,
            ]}>
            <Ionicons
              name={'ios-finger-print-outline'}
              size={wp(5)}
              color={colors.black}
            />
            <MText
              moreStyle={{marginStart: wp(2)}}
              type={'R_3'}
              text={t('quickBalance')}
              color={colors.black}
            />
          </Animated.View>
        </View>
      </View>
    </Layout>
  );
};
const styles = StyleSheet.create({
  HeaderContainer: {
    position: 'absolute',
    top: wp(2),
    width: wp(90),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: wp(5),
    zIndex: 10000,
  },
  TextContainer: {
    marginTop: hp(14),
    alignItems: 'flex-start',
    width: wp(100),
    paddingStart: wp(5),
    zIndex: 1,
  },
  TextFotterContainer: {
    position: 'absolute',
    flexDirection: 'row',
    marginTop: wp(5),
    alignItems: 'center',
    width: wp(88),
    justifyContent: 'center',
  },
});

export default AuthScreen;
