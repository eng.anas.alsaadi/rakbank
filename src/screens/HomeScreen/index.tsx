import React, {useContext} from 'react';
import {View} from 'react-native';
import MButton from '../../components/global/elements/Button';
import MText from '../../components/global/elements/Text';
import colors from '../../constants/colors';
import {AuthContext} from '../../context/AuthContext';

const HomeScreen = ({}) => {
  const {user, logout} = useContext(AuthContext);

  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      {user ? (
        <React.Fragment>
          <MText
            type={'R'}
            text={`User Name: ${user.name}`}
            color={colors.black}
          />
          <MText
            type={'R'}
            text={`User Email: ${user.email}`}
            color={colors.black}
          />
          <MText
            type={'R'}
            text={`User Token: ${user.token}`}
            color={colors.black}
          />
          <MText
            type={'R'}
            text={`User Last Login: ${user.lastLogin}`}
            color={colors.black}
          />

          <MButton text={'logout'} onPress={logout} />
        </React.Fragment>
      ) : null}
    </View>
  );
};

export default HomeScreen;
