import axios from 'axios';

export const config = {
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Cache-Control': 'max-age=31536000',
    },

    timeout: 20000,


};
 

export const ParseFullUrl = (url:string) => {

    return 'https://anasalsaadi.com/api/v1/'+url;

};


const parseErrorRequest = (error:any) => {

    if (error.message === 'Request failed with status code 500') {


        return ({status: 500, data: null, meta: null});
    } else if (error.message === 'Network Error' || error.message === 'timeout of 30000ms exceeded') {
        return ({status: 400, data: null, meta: null});
    } else if (error.response !== undefined) {
        return ({
            status: 403,
            data: error.response.data.data,
            meta: error.response.data.meta,
        });
    } else {
        return ({status: 402, data: null, meta: null});
    }
};
export const requestAction = async (request:any) => {

    console.log('==========================================');
    console.log('type: ', request.type);
    console.log('url: ', request.url);
    console.log('payload: ', JSON.stringify(request.payload));
    console.log('==========================================');

        return new Promise(
            (resolve, reject) => {
                axios.post(ParseFullUrl(request.url),
                    request.payload
                    , config)
                    .then(function (response) {
                        resolve({
                            status: 200,
                            data: response.data.data,
                            meta: response.data.meta,
                        });
                    })
                    .catch(function (error) {
                        console.log('||||||||| |||||||||||||||');
                        console.log(JSON.stringify(error));
                        console.log('||||||||| |||||||||||||||');

                        resolve(parseErrorRequest(error));

                    });
            },
        );
 


};
