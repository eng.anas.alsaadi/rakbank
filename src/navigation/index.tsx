import React, {useContext, useEffect, useState} from 'react';

import {createStackNavigator} from '@react-navigation/stack';
import {NAVIGATION_AUTH_SCREEN, NAVIGATION_HOME_SCREEN} from './types';
import AuthScreen from '../screens/AuthScreen';
import {AuthContext} from '../context/AuthContext';
import HomeScreen from '../screens/HomeScreen/index';
const AppStack = createStackNavigator();
const Stack = createStackNavigator();
const AppNavigator = () => {
  const {user} = useContext(AuthContext);

  return (
    <AppStack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      {user ? (
        <Stack.Screen name={NAVIGATION_HOME_SCREEN} component={HomeScreen} />
      ) : (
        <Stack.Screen name={NAVIGATION_AUTH_SCREEN} component={AuthScreen} />
      )}
    </AppStack.Navigator>
  );
};

export default AppNavigator;
