import React, {useEffect, FC} from 'react';
import Svg, {
  Ellipse,
  Mask,
  G,
  Rect,
  Defs,
  LinearGradient,
  Stop,
} from 'react-native-svg';
import Animated, {
  useSharedValue,
  useAnimatedStyle,
  withRepeat,
  withTiming
} from 'react-native-reanimated';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
const EllipseOne = Animated.createAnimatedComponent(Ellipse);
const GAnimated = Animated.createAnimatedComponent(G);
type Props = {};
const SVGComponent: FC<Props> = ({}) => {
  const scale = useSharedValue(0);
  const config = {
    duration: 3000,
  };
  const reanimatedStyle = useAnimatedStyle(() => {
    return {
      transform: [{translateY: -scale.value}],
    };
  }, []);

  const reanimatedStylePl = useAnimatedStyle(() => {
    return {
      transform: [{translateY: scale.value}],
    };
  }, []);

  useEffect(() => {
      scale.value = withRepeat(withTiming(6, config), -1, true);
  }, []);
  return (
    <Svg
      width={wp(100)}
      height={525}
      viewBox={`0 0 ${wp(100)} 525`}
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <Ellipse
        cx={179.985}
        cy={-40.7384}
        rx={530.015}
        ry={565.262}
        fill="url(#paint0_linear_7773_334432)"
      />
      <Mask
        id="mask0_7773_334432"
        style={{
          maskType: 'alpha',
        }}
        maskUnits="userSpaceOnUse"
        x={-351}
        y={-607}
        width={1061}
        height={1132}>
        <Ellipse
          cx={179.985}
          cy={-40.7384}
          rx={530.015}
          ry={565.262}
          fill="white"
        />
      </Mask>
      <G mask="url(#mask0_7773_334432)">
        <GAnimated opacity={0.705148} style={[reanimatedStyle]}>
          <Ellipse
            cx={116.403}
            cy={108.205}
            rx={366.448}
            ry={390.46}
            fill="url(#paint1_linear_7773_334432)"
          />
          <Ellipse
            cx={116.403}
            cy={108.205}
            rx={366.448}
            ry={390.46}
            fill="#EB151D"
          />
        </GAnimated>
        <Rect
          x={-249.592}
          y={-308.694}
          width={751.404}
          height={827.594}
          stroke="#979797"
          strokeOpacity={0.01}
          strokeWidth={0.905797}
        />
        <EllipseOne
          opacity={0.24714}
          cx={77.5745}
          cy={157.853}
          rx={300.439}
          ry={320.126}
          fill="#FF6A00"
          style={[reanimatedStylePl]}
        />
        <Rect
          x={-282.596}
          y={-100.537}
          width={702.868}
          height={720.023}
          stroke="#979797"
          strokeOpacity={0.8}
          strokeWidth={0.905797}
        />
      </G>
      <Defs>
        <LinearGradient
          id="paint0_linear_7773_334432"
          x1={62.4697}
          y1={-96.4248}
          x2={5.42415}
          y2={538.377}
          gradientUnits="userSpaceOnUse">
          <Stop stopColor="#C10910" />
          <Stop offset={1} stopColor="#9E001F" />
        </LinearGradient>
        <LinearGradient
          id="paint1_linear_7773_334432"
          x1={149.727}
          y1={-672.715}
          x2={-624.956}
          y2={-11.7891}
          gradientUnits="userSpaceOnUse">
          <Stop stopColor="#fff" />
          <Stop offset={1} stopColor="#fff" />
        </LinearGradient>
      </Defs>
    </Svg>
  );
};
export default SVGComponent;
