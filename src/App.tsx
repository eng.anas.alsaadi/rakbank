/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import React from 'react';
import {LogBox} from 'react-native';

import {NavigationContainer} from '@react-navigation/native';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import AppNavigator from './navigation';
import BottomTab from './components/template/BottomTab';
import {Provider} from 'react-redux';
import store from './store';
import LocationProvider from './context/LocationContext';
import DeviceProvider from './context/DeviceContext';
import AuthProvider from './context/AuthContext';
import './localization/i18n';
LogBox.ignoreAllLogs(true);

const App = () => {
  return (
    <Provider store={store}>
      <SafeAreaProvider>
        <LocationProvider>
          <DeviceProvider>
            <AuthProvider>
              <NavigationContainer>
                <AppNavigator />
                <BottomTab />
              </NavigationContainer>
            </AuthProvider>
          </DeviceProvider>
        </LocationProvider>
      </SafeAreaProvider>
    </Provider>
  );
};

export default App;
