export interface User {
    token:string,
    lastLogin:string,
    name:string,
    email:string,
  }

  export type AuthContextType = {
    user:User;
    auth: (payload:any) => void;
    setAuth:(user:User) => void;
    logout:()=>void
  };