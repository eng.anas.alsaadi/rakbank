export interface Location {
    latitude: number;
    longitude: number; 
  }

  export type LocationContextType = {
    currentLocation:Location;
  };