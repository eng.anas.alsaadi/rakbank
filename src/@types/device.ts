export interface Device {
    os: string;
    deviceName: string; 
    mac: string; 
    imei: string; 
    ip: string; 
  }

  export type DeviceContextType = {
    deviceInfo:Device;
  };