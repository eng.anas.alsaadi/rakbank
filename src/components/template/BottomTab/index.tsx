import React, {} from 'react';
import {
  View,
  TouchableWithoutFeedback,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import colors from '../../../constants/colors';
import MText from '../../global/elements/Text';
import Ionicons from 'react-native-vector-icons/Ionicons';
import appStyles from '../../../constants/appStyles';
import SafeAreaView from 'react-native-safe-area-view';
const BottomTab = ({}) => {
  const bottomTabFontSize = 'R_3';
  const bottomTabIconSize = wp(6);

  return (
    <SafeAreaView>
      <View
        style={{
          backgroundColor: colors.white,
          paddingVertical: wp(2),
          borderTopWidth: 1,
          borderTopColor: '#dedad9',
          position: 'relative',
        }}>
        <View
          style={{
            backgroundColor: colors.white,
            width: wp(100),
            flexDirection: 'row',
          }}>
          <TouchableWithoutFeedback>
            <View style={styles.BottomItem}>
              <Ionicons
                name={'wallet-outline'}
                size={bottomTabIconSize}
                color={colors.black}
              />
              <MText
                type={bottomTabFontSize}
                text={'Products'}
                color={colors.black}
                moreStyle={{...styles.fontStyle}}
              />
            </View>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback>
            <View style={styles.BottomItem}>
              <Ionicons
                name={'chatbubble-ellipses-outline'}
                size={bottomTabIconSize}
                color={colors.black}
              />
              <MText
                type={bottomTabFontSize}
                text={'Live chat'}
                color={colors.black}
                moreStyle={{...styles.fontStyle}}
              />
            </View>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback>
            <View style={styles.BottomItem}>
              <Ionicons
                name={'ios-key-outline'}
                size={bottomTabIconSize}
                color={colors.black}
              />
              <MText
                type={bottomTabFontSize}
                text={'RAKToken'}
                color={colors.black}
                moreStyle={{...styles.fontStyle}}
              />
            </View>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback>
            <View style={styles.BottomItem}>
              <Ionicons
                name={'md-location-outline'}
                size={bottomTabIconSize}
                color={colors.black}
              />
              <MText
                type={bottomTabFontSize}
                text={'Location us'}
                color={colors.black}
                moreStyle={{...styles.fontStyle}}
              />
            </View>
          </TouchableWithoutFeedback>
        </View>
      </View>
    </SafeAreaView>
  );
};
const styles = {
  BottomItem: {
    ...appStyles.CenterContent,
    width: wp(25),
  },

  fontStyle: {
    marginTop: wp(1.5),
  },
};

export default BottomTab;
