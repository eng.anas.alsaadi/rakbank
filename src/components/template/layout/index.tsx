import React, {FC, ReactNode, useEffect, useState} from 'react';
import {View, Platform, InteractionManager} from 'react-native';
import colors from '../../../constants/colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import SafeAreaView from 'react-native-safe-area-view';
import SVGComponent from '../../../assets/layout/SVGComponent';
import Animated from 'react-native-reanimated';

type LayoutType = {
  children: ReactNode;
  styleY: any;
};
const Layout: FC<LayoutType> = ({children, styleY}) => {
  const [finished, setDidFinished] = useState<boolean>(false);
  useEffect(() => {
    InteractionManager.runAfterInteractions(() => {
      setTimeout(
        function () {
          setDidFinished(true);
        }.bind(this),
        1,
      );
    });
  }, []);
  return (
    <View style={{flex: 1, backgroundColor: colors.layoutBackgroundColor}}>
      {finished ? (
        <React.Fragment>
          <Animated.View style={[{}, styleY]}>
            <SVGComponent />
          </Animated.View>
          <SafeAreaView
            style={{
              height: Platform.OS === 'android' ? hp(87) : hp(81),
              width: wp(100),
              position: 'absolute',
              paddinBottom:Platform.OS==="android"?hp(13):0,

            }}>
            <View style={{flex: 1, alignItems: 'center'}}>{children}</View>
          </SafeAreaView>
        </React.Fragment>
      ) : null}
    </View>
  );
};

export default Layout;
