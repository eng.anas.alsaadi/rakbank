import React, {FC, ReactNode} from 'react';

import {
  TouchableWithoutFeedback,
  ViewStyle,
  ActivityIndicator,
} from 'react-native';
import colors from './../../../../constants/colors';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import MText from '../Text';
import Animated from 'react-native-reanimated';
type MButtonType = {
  style?: ViewStyle;
  onPress: () => void;
  text?: string;
  type?: string;
  fontType?: string;
  children?: ReactNode;
  loading?: boolean;
};

const MButton: FC<MButtonType> = ({
  style,
  onPress,
  text,
  type,
  fontType,
  children,
  loading,
}) => {
  var VBorderColor = type === 'outline' ? colors.white : 'transparent';
  var VBackgroundColor = type === 'outline' ? 'transparent' : colors.black;
  var fontStyle = fontType ? fontType : 'R';
  var VFontColor = colors.white;
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <Animated.View
        style={[
          {
            borderWidth: 1,
            borderRadius: wp(8),
            borderColor: VBorderColor,
            backgroundColor: VBackgroundColor,
            alignItems: 'center',
            paddingHorizontal: wp(4),
            paddingVertical: wp(3),
            flexDirection: 'row',
            justifyContent: 'center',
            width: wp(80),
            ...style,
          },
        ]}>
        {loading ? (
          <ActivityIndicator color={'#fff'} style={{height: wp(0)}} />
        ) : children ? (
          children
        ) : (
          <MText text={text} type={fontStyle} color={VFontColor} />
        )}
      </Animated.View>
    </TouchableWithoutFeedback>
  );
};

export default MButton;
