import React, {FC, RefObject} from 'react';

import {View, TextInput} from 'react-native';
import colors from './../../../../constants/colors';

import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import MText from '../Text';
type MTextInput = {
  inputRef?: RefObject<TextInput>;
  title: string;
  onChangeText: (value: string) => void;
  value: string;
  secureTextEntry?:boolean
};
const MTextInput: FC<MTextInput> = ({inputRef, title, onChangeText, value,secureTextEntry}) => {
  return (
    <View
      style={{
        backgroundColor: colors.white,
        paddingHorizontal: wp(5),
        paddingVertical: wp(3),
        borderRadius: wp(3),
        marginBottom: wp(4),
      }}>
      <MText type={'R_3'} text={title} color={colors.black} />
      <TextInput
        ref={inputRef}
        secureTextEntry={true} 
        onChangeText={onChangeText}
        value={value}
        style={{
          height: wp(10),
          borderBottomWidth: 1,
          borderBottomColor: colors.yellow,
          backgroundColor: colors.white,
          color: colors.black,
        }}
      />
    </View>
  );
};

export default MTextInput;
