import React, {FC} from 'react';

import {Text, ViewStyle} from 'react-native';
import colors from './../../../../constants/colors';
import typography from '../../../../constants/typography';

type MTextType = {
  text: string;
  type: string;
  center?: boolean;
  right?: boolean;
  left?: boolean;
  color?: string;
  moreStyle?: ViewStyle;
  bold?: boolean;
};
const MText: FC<MTextType> = ({
  text,
  type,
  center,
  left,
  right,
  color,
  moreStyle,
  bold,
}) => {
  const fontArr = [
    {
      type: 'B_9',
      value: typography.TextBold9,
    },
    {
      type: 'R',
      value: typography.TextRegular,
    },
    {
      type: 'R_4',
      value: typography.TextRegular35,
    },
    {
      type: 'R_3',
      value: typography.TextRegular3,
    },
  ];
  const FontType = fontArr.find(e => e.type === type).value;

  const TextAlign = right ? 'right' : left ? 'left' : 'center';
  const FontColor = color ? color : colors.white;
  const fontWeight = bold ? 'bold' : '';

  return (
    <Text
      style={[
        {
          textAlign: TextAlign,
          color: FontColor,
          fontWeight: fontWeight,
          ...FontType,
          ...moreStyle,
        },
      ]}>
      {text}
    </Text>
  );
};

export default MText;
