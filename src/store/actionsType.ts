
export const REQUEST = 'REQUEST';
export const LOADING = 'LOADING';
export const SUCCESS = 'SUCCESS';
export const FAILURE = 'FAILURE';
export const REFRESH = 'REFRESH';

export const LOGIN_REFRESH = 'LoginRefresh';
export const LOGIN_RESET = 'LoginReset';
export const LOGIN_REQUEST = 'LoginRequest';
export const LOGIN_LOADING = 'LoginLoading';
export const LOGIN_SUCCESS = 'LoginSuccess';
export const LOGIN_FAIL = 'LoginFail';
