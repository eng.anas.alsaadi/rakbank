import { LOGIN_REQUEST, LOGIN_RESET } from '../actionsType';

export const login = (payload:any) => ({
    type: LOGIN_REQUEST,
    payload,
});
export const loginReset = (payload:any) => ({
    type: LOGIN_RESET,
    payload,
});