import {takeLatest, call, put} from 'redux-saga/effects';
import {
  LOGIN_FAIL,
  LOGIN_LOADING,
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
} from '../actionsType';
import {requestAction} from './../../api/index';

function* loginAction({payload}: any) {
  try {
    yield put({type: LOGIN_LOADING});

    var requestConfig = {
      type: 'post',
      url: 'login_rak',
      payload: payload,
    };
    const response = yield call(requestAction, requestConfig);

    console.log(response);

    if (response.status === 200) {
      yield put({type: LOGIN_SUCCESS, payload: response.data});
    } else {
      yield put({
        type: LOGIN_FAIL,
        payload: {
          errors: response.meta.errors,
        },
      });
    }
  } catch (error) {
    yield put({
      type: LOGIN_FAIL,
      payload: {
        data: null,
      },
    });
  }
}

export default function* watcherSaga() {
  yield takeLatest(LOGIN_REQUEST, loginAction);
}
