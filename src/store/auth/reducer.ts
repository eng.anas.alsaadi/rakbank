import {
  FAILURE,
  LOADING,
  LOGIN_FAIL,
  LOGIN_LOADING,
  LOGIN_RESET,
  LOGIN_SUCCESS,
  SUCCESS,
} from '../actionsType';

interface ReduxType {
  type: string;
  payload: any;
}

const INITIAL_STATE = {
  isLoadingActionLogin: false,
  statusLogin: '',
  errorsLogin: [],
  userData: null,
};

export default (state = INITIAL_STATE, {type, payload}: ReduxType) => {
  switch (type) {
    case LOGIN_LOADING:
      return {
        ...state,
        isLoadingActionLogin: true,
        statusLogin: LOADING,
        errorsLogin: [],
      };

    case LOGIN_SUCCESS:
      return {
        ...state,
        isLoadingActionLogin: false,
        statusLogin: SUCCESS,
        errorsLogin: [],
        userData: payload,
      };

    case LOGIN_FAIL:
      return {
        ...state,
        isLoadingActionLogin: false,
        statusLogin: FAILURE,
        errorsLogin: payload.errors,
      };

    case LOGIN_RESET:
      return {
        ...state,
        isLoadingActionLogin: false,
        statusLogin: '',
        errorsLogin: [],
      };

    default:
      return state;
  }
};
