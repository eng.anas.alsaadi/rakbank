# `Assessment for RAK BANK`


## Installation

- Clone this repo

	```bash
	git clone git@gitlab.com:eng.anas.alsaadi/rakbank.git
	```

- Install dependencies

	```bash
    cd rakbank
	yarn install
    cd ios 
    pod install 
	```


## Running the app



- ios `react-native run-ios`
- android `react-native run-android`

- for login use this credential 
`User ID: 123` , 
`Password :1234`
